Python библиотека для работы с JSON API Мой Склад
==========================================================================


Requirments
-----------
- requests

Installation
------------
    pip install git+https://github.com/Apocalepse/python-moysklad-api.git

Setup
-----
First, you need to add custom field to your user model to store balance. This field must be ``DecimalField``
with  ``max_digits=8`` and  ``decimal_places=2``.
Example:

.. code:: python

    balance = models.DecimalField(u'Balance', max_digits=8, decimal_places=2, default=0)

SUBSCRIPTION_SUBSCRIBER_MODEL — your user model in format ``app.Model``, default is ``auth.User``

SUBSCRIPTION_SUBSCRIBER_MODEL_FIELD — field in model, specified in SUBSCRIPTION_SUBSCRIBER_MODEL, to store balance.
Default is ``account_balance``

SUBSCRIPTION_SUBSCRIBER_KWARGS — default kwargs for filter accounts (example active, boolean field),
default is ``{'is_active': True}``

Usage
-----
Application has 2 task for celery:

1. make_minus_transactions
Calculating periodic payments for all active subscriptions, creates transactions and disable subscriptions, if need.
Must be run at least once a day.

2. calculate_balances
The task calculates the user's balance, turning over all transactions.
This task also activates the subscription (in the processing of the transaction plus a negative balance in the former,
if enough funds on balance).
Can run as often, how fast you want to adjust the balance and to "see" plus-transaction. For example, every 5 minutes.

Application makes "minus" transactions, plus transactions can be created in code or admin interface.



Create the subscription plan or plans in admin interface.

Application has models for:

Subscription plans,

How you can support this app
----------------------------
Help with documentation, english comments in code and testing. This is something that I do not normally have time to do.
I would appreciate any help and contribution to the project.

