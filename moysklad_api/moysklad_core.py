# -*- coding: utf-8 -*-
import requests
from requests.auth import HTTPBasicAuth


BASE_URL = 'https://online.moysklad.ru/api/remap/1.1'


class _MoySkladCore(object):
    """Абстрактный класс, реализует запрос к api и возвращает результат в json

    """
    def __init__(self, login, password):
        self.login = login
        self.password = password

    @property
    def _api_path(self):
        raise NotImplementedError

    @classmethod
    def meta_href(cls):
        """
        Должен вернуть конечную ссылку для получения объекта из API

        Returns:

        """
        return '{}{}'.format(BASE_URL, cls._api_path)

    def do_request(self, params=None, as_response=False):
        """
        Осуществлят запрос к api по адресу self._api_url с параметрами self.params

        Returns:
            dict: результат ответа
        """
        res = requests.get(
            self.meta_href(),
            params,
            auth=HTTPBasicAuth(self.login, self.password),
        )

        res.raise_for_status()

        return res if as_response else res.json()


class _MoySkladItemList(_MoySkladCore):
    """Абстрактный класс реализует получение списков (вернее генераторов) объектов API
    """

    def __init__(self, limit=100, *args, **kwargs):
        super(_MoySkladItemList, self).__init__(*args, **kwargs)
        self.limit = limit

    offset = 0

    def iter_batches(self):
        """
        Генератор, возвращает пачки размером self.limit с информацией о товарах.

        Returns:
            list: список дисктов с данными о товаре

        """

        while True:
            batch = self.do_request({'limit': self.limit, 'offset': self.offset})
            if batch['rows']:
                self.offset += len(batch['rows'])

                yield batch
            else:
                self.offset = 0
                break

    def all_items_batched(self):
        """Все объекты API

        Генератор, возвращает информацию о всех объектах итерируясь по генератору self.product_batches().
        Метод вернет итератор по по всем объектам, но внутри он получает их пачками по self.limit объектов

        Yields:
            dict: информация о объекте

        """

        for batch in self.iter_batches():
            for p in batch['rows']:
                p['list_meta'] = batch['meta']
                yield p


class MoySkladClient:
    """Класс чтобы запомнить реквизиты для авторизации :)

    """
    def __init__(self, login, password):
        self.login = login
        self.password = password

    def get_iter(self, class_name):
        """
        Вернет идератор по объектам API, описанных классом class_name

        Args:
            class_name (str): имя класса, описывающего объект API

        Yields:
            dict: информация об объекте API

        """
        data_class = class_name(login=self.login, password=self.password)

        return data_class.all_items_batched()

    def get_one(self, meta_href, as_response=False):
        """
        Вернет информацию об одном объекте API по переданному URL

        Args:
            class_name (str): имя класса, описывающего объект API
            meta_href (str): полный URL для получения объекта API

        Returns:
            dict: информация об объекте API

        """
        data_class = MoySkladItem(login=self.login, password=self.password, meta_href=meta_href)

        return data_class.do_request(as_response=as_response)


class MoySkladItem(_MoySkladCore):
    """Класс реализует пролучение информации об одном объекте API по переданному URL

    """
    def __init__(self, meta_href, *args, **kwargs):
        super(MoySkladItem, self).__init__(*args, **kwargs)
        self._meta_href = meta_href

    def meta_href(self):
        return self._meta_href


class Products(_MoySkladItemList):
    """Товары

    """
    _api_path = '/entity/product'


class ProductMeta(_MoySkladItemList):
    """Метаданные товаров

    Атрибуты, типы цен и т.п.
    https://online.moysklad.ru/api/remap/1.1/doc/index.html#%D1%82%D0%BE%D0%B2%D0%B0%D1%80-%D0%BC%D0%B5%D1%82%D0%B0%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D0%B5-%D1%82%D0%BE%D0%B2%D0%B0%D1%80%D0%BE%D0%B2-get

    """
    _api_path = '/entity/product/metadata'


class Entities(_MoySkladItemList):
    """Единицы измерения

    """
    _api_path = '/entity/uom'


class ProductFolders(_MoySkladItemList):
    """Каталоги товаров

    """
    _api_path = '/entity/productfolder'


class Warehouses(_MoySkladItemList):
    """Склады

    """
    _api_path = '/entity/store'


class StocksByStore(_MoySkladItemList):
    """Остатки по складам

    """
    _api_path = '/report/stock/bystore'


class ProductUnits(_MoySkladItemList):
    """Единицы измерения

    """
    _api_path = '/entity/uom'
